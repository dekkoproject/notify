/* Copyright (C) 2016 - 2017 Dan Chapman <dpniel@ubuntu.com>

   This file is part of the Dekko Project

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing,
   software distributed under the License is distributed on an
   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
   either express or implied. See the License for the specific
   language governing permissions and limitations under the License.
*/
import QtQuick 2.4
import Ubuntu.Components 1.3

Item {

    anchors.fill: parent

    property string title
    property string body
    property int position
    property int displayTime
    property Action action
    property string iconName
    property color iconColor

    readonly property bool dockedLeft: position === Item.Left
    readonly property bool dockedRight: position === Item.Right
    readonly property bool dockedCenter: position === Item.Center
    readonly property bool hasIcon: iconName

    signal closed()
}
